# Warner Bros Code Challenge

Summary: This is my submission for the Warner Bros. Digital Labs code challenge. To run this application, you can either use the Angular CLI command `ng serve -o` or you can host the dist files in a container, virtual machine or server of your choice. To aquire the dist files use `ng build --prod`.

Home page: The home page plays a fun bit of the Duck Dodgers and the 24th and 1/2 Century episode of Looney Tunes.

Find Marvin Game: The Find Marvin Game allows you to play as Duck Dodgers as you try to disintegrate Marvin the Martian before he disintegrates you.

About Page: The About Page lists some development details for the perusal of whoever reviews the project. It offers some insight into some of my techniques and the design choices made on this project. 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
