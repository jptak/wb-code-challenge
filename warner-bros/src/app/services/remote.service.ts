import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RemoteService {

  constructor(private http: HttpClient) {}

  /*
  Makes a get request to NASA's API to retrieve the Astronmical Picture of the Day (APOD)
   */
  public retrieveNasaPictureOfTheDay(dateString: string) {
    // Add query string parameters here
    let parametersObject = new HttpParams({fromObject: { api_key: environment.nasaKey, date: dateString}});
    const options = {
      params: parametersObject
    };
    // Get request made here.
    return this.http.get(environment.nasaPictureOfTheDay,  options).pipe(
      map(res => {
        const imageUrl = res['hdurl'];
        return imageUrl;
      })
    );
  }
}
