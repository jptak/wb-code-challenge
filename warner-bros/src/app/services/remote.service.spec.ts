import { TestBed } from '@angular/core/testing';

import { RemoteService } from './remote.service';
import {HttpClient} from '@angular/common/http';
import {inject} from '@angular/core/testing';
import {of} from 'rxjs';

class MockHttpClient {
  get(url, options) {
    return of({
      "copyright": "Apologies to: AstronomerGeographer Image Pixelation: Rob Stevenson",
      "date": "2015-06-16",
      "explanation": "Welcome to the vicennial year of the Astronomy Picture of the Day! Perhaps a source of web consistency for some, APOD is still here.  As during each of the 20 years of selecting images, writing text, and editing the APOD web pages, the occasionally industrious Robert Nemiroff (left) and frequently persistent Jerry Bonnell (right) are pictured above plotting to highlight yet another unsuspecting image of our cosmos. Although the featured image may appear similar to the whimsical Vermeer composite that ran on APOD's fifth anniversary, a perceptive eye might catch that it has been digitally re-pixelated using many of the over 5,000 APOD images that have appeared over APOD's tenure.  (Can you find any notable APOD images?) Once again, we at APOD would like to offer a sincere thank you to our readership for continued interest, support, and many gracious communications.   20th Anniversary Interview: An oral history of Astronomy Picture of the Day",
      "hdurl": "https://apod.nasa.gov/apod/image/1506/vermeer_stevenson_3000.jpg",
      "media_type": "image",
      "service_version": "v1",
      "title": "APOD is 20 Years Old Today",
      "url": "https://apod.nasa.gov/apod/image/1506/vermeer_stevenson_960.jpg"
    });
  }
}

describe('RemoteService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      RemoteService,
      { provide: HttpClient, useClass: MockHttpClient}
    ]
  }));

  it('should be created', inject([RemoteService], (service: RemoteService) => {
    expect(service).toBeTruthy();
  }));
  it('should take in options, filter the retrieved observable and return it', inject([RemoteService], (service: RemoteService) => {

    let result = service.retrieveNasaPictureOfTheDay('1995-06-16');
    result.subscribe((res)=>{
      expect(res).toBe('https://apod.nasa.gov/apod/image/1506/vermeer_stevenson_3000.jpg');
    });
  }));

});
