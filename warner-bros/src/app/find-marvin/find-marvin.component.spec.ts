import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindMarvinComponent } from './find-marvin.component';
import {RemoteService} from '../services/remote.service';
import {DatePipe} from '@angular/common';
import {of} from 'rxjs';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

class MockRemoteService {
  retrieveNasaPictureOfTheDay() {
    return of('test');
  }
}

class MockDatePipe {
  transform(value) {
    return '1995-06-16';
  }
}

let timerCallback;

describe('FindMarvinComponent', () => {
  let component: FindMarvinComponent;
  let fixture: ComponentFixture<FindMarvinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindMarvinComponent ],
      providers: [
        {provide: RemoteService, useClass: MockRemoteService},
        {provide: DatePipe, useClass: MockDatePipe}
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindMarvinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    timerCallback = jasmine.createSpy("timerCallback");
    jasmine.clock().install();
  });

  afterEach(() => {
    jasmine.clock().uninstall();
  });

  it('should create Find Marvin Component Successfully', () => {
    expect(component).toBeTruthy();
  });

  it('should generate a random number between a given min and max', () => {
    // expect(component).toBeTruthy();
    const minValues = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    const maxValues = [100, 101, 102, 103, 104, 105, 106, 107, 108, 109];
    for (let i = 0; i < minValues.length; i++) {
      let result = component.randomValue(minValues[i], maxValues[i]);
      expect(result).toBeTruthy();
      expect(result).toBeGreaterThanOrEqual(minValues[i]);
      expect(result).toBeLessThanOrEqual(maxValues[i]);
    }
  });

  it('should reset the game values', () => {
    component.playerWon = true;
    component.playerLost = true;
    component.gameStarted = true;
    component.timeLeft = 0;

    component.resetGame();

    expect(component.playerWon).toBeFalsy();
    expect(component.playerLost).toBeFalsy();
    expect(component.gameStarted).toBeFalsy();
    expect(component.timeLeft).toEqual(component.timeLimit);
  });

  it('should activate the stop-game process correctly.', () => {
    spyOn(component, 'resetGame');
    spyOn(component, 'resetMessageBox');
    component.stopPlaying();
    expect(component.resetGame).toHaveBeenCalledTimes(1);
    expect(component.resetMessageBox).toHaveBeenCalledTimes(1);
  });

  it('should call randomizeValue when YAxis is called', () => {
    spyOn(component, 'randomValue');
    component.randomizeYAxis();
    expect(component.randomValue).toHaveBeenCalledTimes( 1);
  });

  it('should call randomizeValue when XAxis is called', () => {
    spyOn(component, 'randomValue');
    component.randomizeXAxis();
    expect(component.randomValue).toHaveBeenCalledTimes(1);
  });

  it('should randomize a date of the format yyyy-MM-dd', () => {
    spyOn(component, 'randomValue');
    let result = component.randomizeDate();
    expect(result).toBeTruthy();
    expect(component.randomValue).toHaveBeenCalledTimes(1);
  });

  it('should accurately react to player win-condition depending on whether the player has lost or not', () => {
    // branch #1 - player has already lost and clicks on Marvin
    component.playerWon = false;
    component.playerLost = true;
    component.playerFoundMarvin();
    expect(component.playerWon).toBeFalsy();
    // branch #2 - player has found Marvin before the time runs out and clicks on Marvin
    component.playerLost = false;
    component.playerFoundMarvin();
    expect(component.playerWon).toBeTruthy();
  });

  it('should accurately react to player loss-condition where time has run out.', () => {
    component.playerLost = false;
    component.playerFailedToFindMarvinInTime();
    expect(component.playerLost).toBeTruthy();
  });

  it('should correctly start the timer and tick down the interval with each passing second.', () => {
    // Reset basic values
    component.resetGame();
    // spy on the player failed method in case the time runs out
    spyOn(component, 'playerFailedToFindMarvinInTime');
    // set a basic time limit that will allow us to test all branches and functionality
    component.timeLimit = 3;
    component.timeLeft = 3;
    // start the system under test
    component.startCountdown();
    // tick the clock down and check the time left at each interval.
    expect(component.timeLeft).toBe(3);
    jasmine.clock().tick(1001);
    expect(component.timeLeft).toBe(2);
    jasmine.clock().tick(1001);
    expect(component.timeLeft).toBe(1);
    jasmine.clock().tick(1001);
    expect(component.timeLeft).toBe(0);
    jasmine.clock().tick(1001);
    // check to make sure that when the time runs out, the failure method has been called.
    expect(component.playerFailedToFindMarvinInTime).toHaveBeenCalledTimes(1);
  });

  it('should retrieve the value inside the observable returned from the remote service.', () => {
    component.imageUrl = '';
    component.getNewImage();
    expect(component.imageUrl).toBeTruthy();
    expect(component.imageUrl).toBe('test');
  });

  it('should go through the game startup sequence and call the appropriate helper methods.', () => {
    spyOn(component, 'getNewImage');
    spyOn(component, 'resetGame');
    spyOn(component, 'randomizeXAxis');
    spyOn(component, 'randomizeYAxis');
    spyOn(component, 'startCountdown');
    component.gameStarted = false;
    component.startGame();
    expect(component.gameStarted).toBeTruthy();
    expect(component.getNewImage).toHaveBeenCalledTimes(1);
    expect(component.resetGame).toHaveBeenCalledTimes(1);
    expect(component.randomizeXAxis).toHaveBeenCalledTimes(1);
    expect(component.randomizeYAxis).toHaveBeenCalledTimes(1);
    expect(component.startCountdown).toHaveBeenCalledTimes(1);
  });
});
