import {Component, Input, OnInit} from '@angular/core';
import {RemoteService} from '../services/remote.service';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-find-marvin',
  templateUrl: './find-marvin.component.html',
  styleUrls: ['./find-marvin.component.scss']
})
export class FindMarvinComponent implements OnInit{

  imageUrl;
  marvinXAxis;
  marvinYAxis;
  playerWon: boolean;
  playerLost: boolean;
  gameStarted: boolean;
  timeInterval;
  timeLimit: number = 9;
  timeLeft: number = this.timeLimit;
  messageBoxTitle = '';
  messageBoxContent = '';

  constructor(private remoteService: RemoteService, private datePipe: DatePipe) {
    // a starting image for the game's background.
    this.imageUrl = 'https://apod.nasa.gov/apod/image/1911/SuperSpirals_HubbleSDSS_1777.jpg';
    this.playerWon = false;
    this.playerLost = false;
    this.gameStarted = false;
  }

  /*
  Starts the game by setting all the relevant variables to their proper states.
  Also begins the countdown timer.
   */
  startGame() {
    this.getNewImage();
    this.resetGame();
    this.marvinXAxis = this.randomizeXAxis();
    this.marvinYAxis = this.randomizeYAxis();
    this.gameStarted = true;
    this.startCountdown();
  }

  /*
  Gets new image from the NASA API which is then used as the background for the game.
   */
  getNewImage() {
    const newDate = this.randomizeDate();
    this.remoteService.retrieveNasaPictureOfTheDay(newDate).subscribe(
      (res) => {
        this.imageUrl = res;
      }
    );
  }

  /*
  Randomizes a date to b e within the range needed for NASA's API.
   */
  randomizeDate() {
    const startDate = new Date('06/16/1995').getTime();
    const endDate = new Date().getTime();
    const newDate = this.datePipe.transform(new Date(this.randomValue(startDate, endDate)), 'yyyy-MM-dd');
    return newDate;
  }

  /*
  Creates a random value between a given min and max.
   */
  randomValue(min, max) {
    const result = Math.random() * (max - min) + min;
    return result;
  }

  /*
  Sets parameters for randomizing where Marvin can appear on the Y Axis of the page.
   */
  randomizeYAxis() {
    return this.randomValue(0, 80);
  }

  /*
  Sets parameters for randomizing where Marvin can appear on the X Axis of the page.
   */
  randomizeXAxis() {
    return this.randomValue(0, 80);
  }

  /*
  Reset's the component's attributes to prepare for a new game.
   */
  resetGame() {
    this.playerWon = false;
    this.playerLost = false;
    this.gameStarted = false;
    this.timeLeft = this.timeLimit;
  }
  /*
  Reset game and message box when the stop button is clicked.
   */
  stopPlaying(){
    this.resetGame();
    this.resetMessageBox();
  }

  resetMessageBox() {
    this.messageBoxTitle = 'Disintegrate Marvin!';
    this.messageBoxContent = 'You, Duck Dodgers, are all that is stopping Marvin from claiming Planet X! Search space by hovering your mouse around to look for Marvin. Click on him to disintegrate him! Hurry! Marvin is looking for you too!';
  }

  /*
  Stop's the timer
  Set's the player as the winner if the player has not already lost.
   */
  playerFoundMarvin() {
    this.gameStarted = false;
    clearInterval(this.timeInterval);
    if(!this.playerLost) {
      this.playerWon = true;
      this.messageBoxTitle = 'You Won!';
      this.messageBoxContent = 'You beat Marvin on the draw!';
    }
  }

  /*
  Stop's the timer
  Set's the player as the loser
   */
  playerFailedToFindMarvinInTime() {
    this.gameStarted = false;
    this.playerLost = true;
    this.messageBoxTitle = 'You Lost!';
    this.messageBoxContent = 'Marvin found you first!';
    clearInterval(this.timeInterval);
  }

  /*
  Starts's the timer
   */
  startCountdown() {
    this.timeInterval = setInterval(() => {
      if(this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.playerFailedToFindMarvinInTime();
      }
    }, 1000);
  }

  ngOnInit(): void {
    this.resetMessageBox();
  }
}
