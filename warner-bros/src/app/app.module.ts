import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { FindMarvinComponent } from './find-marvin/find-marvin.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { AboutPageComponent } from './about-page/about-page.component';
import {RemoteService} from './services/remote.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {DatePipe} from '@angular/common';
import { InfoBoxComponent } from './info-box/info-box.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    FindMarvinComponent,
    PageNotFoundComponent,
    NavigationComponent,
    AboutPageComponent,
    InfoBoxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ RemoteService, HttpClient, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
