import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomePageComponent} from './home-page/home-page.component';
import {FindMarvinComponent} from './find-marvin/find-marvin.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {AboutPageComponent} from './about-page/about-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'find-marvin', component: FindMarvinComponent},
  { path: 'about', component: AboutPageComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
