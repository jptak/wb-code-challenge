import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoBoxComponent } from './info-box.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('InfoBoxComponent', () => {
  let component: InfoBoxComponent;
  let fixture: ComponentFixture<InfoBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoBoxComponent ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit the start game event properly.', () => {
    spyOn(component.startGameEvent, 'emit');
    component.emitStartGameEvent();
    expect(component.startGameEvent.emit).toHaveBeenCalledTimes(1);
  });

  it('should emit the reset game event properly.', () => {
    spyOn(component.resetGameEvent, 'emit');
    component.emitResetGameEvent();
    expect(component.resetGameEvent.emit).toHaveBeenCalledTimes(1);
  });
});
