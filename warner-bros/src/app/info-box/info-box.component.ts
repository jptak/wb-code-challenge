import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-info-box',
  templateUrl: './info-box.component.html',
  styleUrls: ['./info-box.component.scss']
})
export class InfoBoxComponent {

  @Input() gameStarted: boolean;
  @Input() title: string;
  @Input() message: string;
  @Output() startGameEvent: EventEmitter<string>;
  @Output() resetGameEvent: EventEmitter<string>;
  constructor() {
    this.startGameEvent = new EventEmitter<string>();
    this.resetGameEvent = new EventEmitter<string>();
  }
  /*
  Emit event to start game
   */
  emitStartGameEvent() {
    this.startGameEvent.emit('start');
  }
  /*
  Emit event to stop game
   */
  emitResetGameEvent() {
    this.resetGameEvent.emit('reset');
  }
}
