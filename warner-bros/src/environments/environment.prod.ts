export const environment = {
  production: true,
  nasaPictureOfTheDay: 'https://api.nasa.gov/planetary/apod',
  nasaKey: '--key--'
};
